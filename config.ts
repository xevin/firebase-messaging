export const firebaseConfig = {
  // Your web app's Firebase configuration here
  // See https://firebase.google.com/docs/web/setup#add-sdks-initialize
  apiKey: 'AIzaSyCu2mNsC_GmV6JI44pZFkRzgogMukp-4KI',
  authDomain: 'autoinline-auth.firebaseapp.com',
  databaseURL: 'https://autoinline-auth.firebaseio.com',
  projectId: 'autoinline-auth',
  storageBucket: 'autoinline-auth.appspot.com',
  messagingSenderId: '446649179089',
  appId: '1:446649179089:web:e07e8e11466dfb8b441ef0',
  // measurementId: 'G-MEASUREMENT_ID'
};

export const vapidKey = 'BDOU99-h67HcA6JeFXHbSNMu7e2yNNu3RzoMj8TM4W88jITfq7ZmPvIM1Iv-4_l2LxQcYwhqby2xGpWwzjfAnG4';
